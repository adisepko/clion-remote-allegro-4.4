# clion-remote-allegro-4.4

## Description

Clion project that allows you to develop with Allegro library remotly.


![Alt Text](readme/usage.gif)

## Installation (For Windows)

#### 1. Set up X server on windows

Install x server. I like the x server from **VcXsrv** but there are others you can try like X Ming.

    choco install vcxsrv

Launch the x server. Run XLaunch from start menu, or if not found, try looking for xlaunch.exe at the default install
location “C:\Program Files\VcXsrv\xlaunch.exe”

Go with all the default settings, however **do note to check “Disable access control”**


![img.png](readme/launchX.png)

#### 2. Build docker image (docker required)

Go to directory with **Dockerfile** and run below command from command line or Windows PowerShell

    docker build -t gk/remote-allegro-4.4-env:0.1 -f Dockerfile .

#### 3. Configure Clion

- Set up toolchain in Clion settings (press ctrl + shift + a and type toolchains)
- Add new docker toolchain

![img.png](readme/new_docker_toolchain.png)
- Create new docker server 

![img.png](readme/create_new_docker_server.png)
- Wait for Clion to autodetect the image and settings (it may take a
  while)

![img.png](readme/docker_toolchain_settings.png)
- Set container settings like on the screenshot
  below 

![img.png](readme/container_settings.png)
- Make sure that your new config is on the top and save the changes
