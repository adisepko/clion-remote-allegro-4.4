cmake_minimum_required(VERSION 3.16)
project(lab1)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES main.cpp)
add_executable(lab1 ${SOURCE_FILES})

INCLUDE_DIRECTORIES( /usr/include/allegro/ )
LINK_DIRECTORIES(  /usr/lib/x86_64-linux-gnu/)
file(GLOB LIBRARIES "/usr/lib/x86_64-linux-gnu/liball*")
message("LIBRARIES = ${LIBRARIES}")

TARGET_LINK_LIBRARIES(lab1  ${LIBRARIES})